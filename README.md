# Capillarity

The main objective of this project is to present topics related to Capillarity.

## Introduction

How to explain the formation of water droplets? Why do wet hairs stick together? Why does soap help with cleaning? How can insects and needles stay on water?
Understanding these issues better is one of our main motivations.

<img src = "imgs/Fig5.png" width = "600">

It is interesting to mention the distinction between a needle and a boat on the water.
Density has a fundamental role in this matter. In a portion of water consider any imaginary closed surface.
If that portion of water remains at rest, it is because the resulting force on it is zero. Only the force of gravity and interactions with water outside the border act on it.

$`\vec{P_w} + \vec{E} = \vec{0} \implies \vec{E} = -m_w \vec{g}`$

But these contact forces, which give rise to the buoyancy, will not be different if the material inside that border is different.

$`\sum{\vec{F}} = \vec{P} + \vec{E} = (m - m_w)\vec{g} = (\rho - \rho_w) V \vec{g}`$

The volume is the same in both cases.

In this model, density determines whether the object floats or sinks.

<img src = "imgs/Fig9.png" width = "250">



The boat floats because its density is lower than the water density. It remains on water due to the buoyant force.
The needle, on the other hand, is completely filled with metal, being more dense than water. And with a little push it will sink as expected from the start. Therefore, its permanence on the surface cannot be explained by the buoyancy, and the force that supports its weight results from a different phenomenon.

## Origin

Water is the main substance in which the capillary effect is usually observed.

The parts that make up the liquid form connections with each other. However, particles at the interface can only make half the number of connections.

<img src = "imgs/Fig8.png" width = "428"> <img src = "imgs/Fig6.png" width ="428">    



So, the <b>Cohesion Energy</b> of a particle  in the middle of the liquid, is given by:

$`U \approx n U_l`$

- $`n`$ is the number of bounds.

- $`U_l `$ is the <b>Binding Energy</b>.
  - $`U_l = k_B T`$
  - $`k_B = 1.38064852 × 10^{-23} \frac{J}{K}`$ is the Boltzmann constant.
  - $`U_l \approx \frac{1}{40} eV`$ for $`T = 300K`$

For a molecule at the interface:

$`\Delta U \simeq \frac{n}{2} U_l`$

The <b>Surface Energy</b> will e proportional to the surface area.

$`\boxed{U_\Sigma = \gamma \Sigma}`$

$`\gamma`$  is the <b>Surface Tension</b>. In the International System $`[\gamma] = \frac{J}{m^2}`$.

We can find the relation between the Surface Energy and the Binding Energy.

$`U_\Sigma = \sum_{i=1}^N [\frac{n}{2} U_l] = \frac{n}{2} U_l \sum_{i=1}^N 1 = \frac{n}{2} U_l N `$

$`N`$ is the total number of surface particles. Evaluating the molecule area by $`a^2`$, we have:

$`N \simeq \frac{\Sigma}{a^2}`$

We can imagine that each molecule occupies a square with side $`a`$.

The new relation for $`U_\Sigma`$ is:

$`\boxed{U_\Sigma = \frac{\frac{n}{2} U_l}{a^2} \Sigma}`$ 

Here we can see the relation between Surface Tension $`\gamma`$ and the characteristics of the molecules.

$`\gamma \simeq \frac{\frac{n}{2} U_l}{a^2}`$



## Energy and Work

To increase the surface area, work must be done to break connections in the medium of the fluid. We can find the energy required to increase the area by a amount $`d\Sigma`$.

$`U_\Sigma = \gamma \Sigma \implies \boxed{dU_\Sigma = \gamma d\Sigma} `$

Taking a simple case, like the one in the image below, where the varied area has a rectangular shape and one side of this rectangle has a fixed length $`l`$.

$`d\Sigma = l dx`$

In this case, the <b>Capillary Force</b> will be:

$`\boxed{\vec\gamma = - \frac{dU_{\Sigma}}{dx} \hat{x} = -\gamma l \hat{x}}`$

<img src = "imgs/Fig 6.png" width = "428 ">

### Laplace's Theorem

The pressure difference between fluids is given by:

$`\boxed{\Delta p = \gamma (\frac{1}{R} + \frac{1}{R'}) = \gamma C}`$

Where $`C = \frac{1}{R} + \frac{1}{R'}`$ is the <b>Curvature Radius</b> of the surface that borders the fluids.

For more about this subject read the recommendations section.

As a particular case, if the surface is spherical $`R = R'`$. Then $`C = \frac{2}{R}`$ and finally:

$`\Delta p = \frac{2 \gamma}{R} `$ 



<img src = "imgs/Drop Area Variation.png" width ="550">

 

## Recommendations 

- The channel [Faculty of Khan](https://www.youtube.com/channel/UCGDanWUzNMbIV11lcNi-yBg) has a playlist about Differential Geometry. For our subject, the video on [Curvature](https://www.youtube.com/watch?v=URr6ZXhZMJY&list=PLdgVBOaXkb9DJjk8V0-RkXTnD4ZXUOFsc&index=5) can be of great help.
- There are useful introductory videos by [Victor Ugaz](https://www.youtube.com/channel/UC2OlDnoOOBYj5BkEsl5H9Dg). The [part 1](https://www.youtube.com/watch?v=YdLeLvNfE-w) is an introduction and the [part 2](https://www.youtube.com/watch?v=PezcU4hHML4) is about Young-Laplace equation.
- I useful video about [Contact Angle](https://www.youtube.com/watch?v=9WigcUFS3eQ).

- To study <b>Minimal Surfaces</b> more deeply we need to use <b>Calculus of Variations</b>. This [video](https://www.youtube.com/watch?v=V0wx0JBEgZc) is a good introduction and this [playlist](https://www.youtube.com/watch?v=6HeQc7CSkZs&list=PLdgVBOaXkb9CD8igcUr9Fmn5WXLpE8ZE_) is very helpful to get many important ideas.

